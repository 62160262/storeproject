/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class Receipt {
    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private ArrayList<ReceiptDetail>receiptdetail;

    public Receipt(int id, Date created, User seller, Customer customer, ArrayList<ReceiptDetail> receiptdetail) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        this.receiptdetail = receiptdetail;
    }
    
    
}
