package com.natruja.storeproject.poc;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class TestInsertProduct {

    private static String dbPath;

    public static void main(String[] args) {
        Connection con = null;
        String daPath = "./db/store.db";
        try {
            Class.forName("org.Sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite" + dbPath);
            System.out.println("Database connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error : JDBC is not exist ");
        } catch (SQLException ex) {
            System.out.println("Error : Databace cannot connection ");
        }
        try {
            String sql = "IINSERT INTO product (name,price)VALUES (45,'Americano', 1); ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, "Late");
            stmt.setDouble(2, 50);
            int row = stmt.executeUpdate();
            System.out.println("Affect now " + row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);

            }
            System.out.println("Affect " + row + " " + "id " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error : Cannot clase database");
        }

    }

}
